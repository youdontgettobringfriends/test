Ext.define('TestModule.store.Users', {
    extend: 'Ext.data.Store',
    alias: 'store.users',

    model: 'TestModule.model.User',
    pageSize: 5,

    proxy: {
        type: 'ajax',
        url: '/api/users',
        filterParam: 'filter',
        reader: {
            type: 'json',
            rootProperty: 'users',
            totalProperty: 'total',
        }
    },

    autoLoad: true,
    remoteFilter: true,
    remoteSort: true
});