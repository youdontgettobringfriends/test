Ext.define('TestModule.view.main.Main', {
    extend: 'Ext.Panel',
    xtype: 'users',
    controller: 'main',

    requires: [
        'Ext.grid.Grid',
        'Ext.layout.Fit'
    ],


    layout: 'fit',

    items: [{
        xtype: 'grid',
        id: 'main-grid',

        store: {
            type: 'users'
        },

        plugins: {
            pagingtoolbar: true
        },

        items: [
            {
                xtype: 'toolbar',
                docked: 'top',
                items : [
                    {
                        xtype: 'textfield',
                        label: 'IP адрес',
                        id: 'ip-input'
                    }, {
                        xtype: 'button',
                        text: 'Search',
                        listeners: {
                            tap: function (e) {
                                var searchValue = Ext.getCmp('ip-input').getValue();
                                var store = Ext.getCmp('main-grid').store;
                                store.filter({
                                    property     : 'ip',
                                    value         : searchValue,
                                });
                            }
                        }
                    }
                ]
            }
        ],
        columns: [{
            text: 'IP',
            dataIndex: 'ip',
            filter: {
                type: 'string',
            }
        }, {
            text: 'Browser',
            dataIndex: 'browser',
        }, {
            text: 'OS',
            dataIndex: 'os'
        }, {
            text: 'Урл с которого зашел первый раз',
            dataIndex: 'first_from_url',
            sortable: false,
            flex: 1
        }, {
            text: 'Урл на который зашел последний раз',
            dataIndex: 'last_to_url',
            sortable: false,
            flex: 1
        }, {
            text: 'кол-во уникальных посещенных урл',
            dataIndex: 'unique_urls',
            sortable: false
        }],
    }],
});