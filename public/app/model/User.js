Ext.define('TestModule.model.User', {
    extend: 'Ext.data.Model',

    fields: [{
        name: 'ip',
        type: 'string'
    }, {
        name: 'browser',
        type: 'string'
    }, {
        name: 'os',
        type: 'string'
    }, {
        name: 'first_from_url',
        type: 'string'
    }, {
        name: 'last_to_url',
        type: 'string'
    }, {
        name: 'unique_urls',
        type: 'string'
    }]
});