Ext.define('TestModule.TestApp', {
    extend: 'Ext.app.Application',

    mainView: 'TestModule.view.main.Main',
});