<?php

use Zend\Mvc\Application;
use Zend\Stdlib\ArrayUtils;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use TestModule\Model\User;
use TestModule\Model\UserVisit;
use TestModule\Model\Table\UserTable;
use TestModule\Model\Table\UserVisitTable;
use TestModule\Services\LogsParserService;

include __DIR__ . '/../../../vendor/autoload.php';

$appConfig = require __DIR__ . '/../../../config/application.config.php';
if (file_exists(__DIR__ . '/../../../config/development.config.php')) {
    $appConfig = ArrayUtils::merge($appConfig, require __DIR__ . '/../../../config/development.config.php');
}


$app = Application::init($appConfig);
$container = $app->getServiceManager();
$dbAdapter = $container->get(AdapterInterface::class);


$userTable = $container->get(UserTable::class);
$userVisitTable = $container->get(UserVisitTable::class);

$logsParser = new LogsParserService();
$users = $logsParser->getUsers();


foreach ($users as $user) {
    $userTable->saveUser($user);
}

$visits = $logsParser->getVisits($userTable->fetchAll());

foreach ($visits as $visit) {
    $userVisitTable->saveUserVisit($visit);
}




