<?php

namespace TestModule;

use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'index' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/',
                    'defaults' => [
                        'controller' => Controller\TestModuleController::class,
                        'action'     => 'index'
                    ]
                ],
            ],
            'users' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/api/users',
                    'defaults' => [
                        'controller' => Controller\ApiController::class,
                        'action' => 'fetchUsers'
                    ]
                ]
            ]
        ]
    ],

    'view_manager' => [
        'template_path_stack' => [
            'test' => __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy'
        ]
    ],
];