<?php

namespace TestModule\Model\Table;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\Sql\Where;
use TestModule\Model\User;

class UserTable
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        return $this->tableGateway->select();
    }

    public function fetchUsersWithVisitsAggregated($start = 0, $limit = 5, $sort = 'id', $direction = 'DESC', $ip = null)
    {
        $direction = preg_match("/^(ASC|DESC)$/i", $direction) ? $direction : 'ASC';
        $sort = in_array($sort, array_keys(get_class_vars(User::class))) ? $sort : 'id';

        $sql = 'SELECT "user".*, (SELECT COUNT(*) FROM (SELECT url_from as url, user_id FROM user_visit WHERE user_id="user".id UNION SELECT url_to as url, user_id FROM user_visit WHERE user_id="user".id) as urls) as unique_urls, (SELECT url_from FROM user_visit WHERE user_visit.id=(SELECT MIN(id) FROM user_visit WHERE user_id="user".id)) as first_from_url, (SELECT url_to FROM user_visit WHERE user_visit.id=(SELECT MAX(id) FROM user_visit WHERE user_id="user".id)) as last_to_url FROM "user" WHERE ip LIKE ? ORDER BY  '. "$sort $direction" . ' LIMIT ? OFFSET ?';

        $result = $this->tableGateway->getAdapter()->query($sql);
        $result = $result->execute([$ip . '%', (int)$limit, (int)$start]);

        /*
            Отдаем результат в виде привязанного к текущему классу ResultSet
        */
        $resultSet = clone $this->tableGateway->getResultSetPrototype();
        $resultSet->initialize($result);

        return $resultSet;
    }

    public function count($ip = null)
    {
        $where = (new Where())->like('ip', $ip . '%');
        $result =  $this->tableGateway->select($where);

        return $result->count();
    }

    public function getUser($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (! $row) {
            throw new RuntimeException(sprintf(
                'Could not find row with identifier %d',
                $id
            ));
        }

        return $row;
    }

    public function saveUser(User $user)
    {
        $data = [
            'ip' => $user->ip,
            'browser'  => $user->browser,
            'os' => $user->os
        ];

        $id = (int) $user->id;

        if (!$id) {
            $this->tableGateway->insert($data);
            return $this->tableGateway->lastInsertValue;
        }

        if (! $this->getUser($id)) {
            throw new RuntimeException(sprintf(
                'Cannot update user with identifier %d; does not exist',
                $id
            ));
        }

        $this->tableGateway->update($data, ['id' => $id]);
    }

    public function deleteUser($id)
    {
        $this->tableGateway->delete(['id' => (int) $id]);
    }
}