<?php

namespace TestModule\Model\Table;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;
use TestModule\Model\UserVisit;

class UserVisitTable
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $this->tableGateway->select();
    }

    public function getUserVisit($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (! $row) {
            throw new RuntimeException(sprintf(
                'Could not find row with identifier %d',
                $id
            ));
        }

        return $row;
    }

    public function saveUserVisit(UserVisit $visit)
    {
        $data = [
            'user_id' => $visit->user_id,
            'visited_at'  => $visit->visited_at,
            'url_from' => $visit->url_from,
            'url_to' => $visit->url_to
        ];

        $id = (int) $visit->id;

        if (!$id) {
            $this->tableGateway->insert($data);
            return;
        }

        if (! $this->getVisit($id)) {
            throw new RuntimeException(sprintf(
                'Cannot update visit with identifier %d; does not exist',
                $id
            ));
        }

        $this->tableGateway->update($data, ['id' => $id]);
    }

    public function deleteVisit($id)
    {
        $this->tableGateway->delete(['id' => (int) $id]);
    }
}