<?php

namespace TestModule\Model;

class UserVisit
{
    public $id;
    public $user_id;
    public $visited_at;
    public $url_from;
    public $url_to;

    public function exchangeArray(array $data)
    {
        $this->id = !empty($data['id']) ? $data['id'] : null;
        $this->user_id = !empty($data['user_id']) ? $data['user_id'] : null;
        $this->visited_at = !empty($data['visited_at']) ? $data['visited_at'] : null;
        $this->url_from = !empty($data['url_from']) ? $data['url_from'] : null;
        $this->url_to = !empty($data['url_to']) ? $data['url_to'] : null;
    }
}