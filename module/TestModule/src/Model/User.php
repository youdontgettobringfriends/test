<?php

namespace TestModule\Model;

class User
{
    public $id;
    public $ip;
    public $browser;
    public $os;

    private $data = [];

    public function exchangeArray(array $data)
    {
        foreach (get_class_vars(self::class) as $var => $value) {
            if (!isset($data[$var]) && $var !== 'data') {
                throw new \RuntimeException("Не указано обязательное значение поля {$var} для пользователя");
            }
        }

        foreach ($data as $column => $value) {
            $this->$column = $value;
        }
    }

    /*
        Воспользуемся перегруженными свойствами для различных данных из БД, которые не имеют соответствующего поля у класса пользователя
    */
    public function __set($propertyName, $propertyValue)
    {
        $this->data[$propertyName] = $propertyValue;
    }


    public function __get($property)
    {
        if (isset($this->data[$property])) {
            return $this->data[$property];
        }

        return null;
    }

    public function toArray()
    {
        $result = [];

        foreach (array_merge(get_object_vars($this), $this->data) as $var => $value) {
            $result[$var] = $value;
        }

        unset($result['data']);

        return $result;
    }
}