<?php
namespace TestModule\Services;

use TestModule\Model\User;
use TestModule\Model\UserVisit;

class LogsParserService
{
    public function getUsers()
    {
        $data = file_get_contents(__DIR__ . "/../../logs/users.log");
        $data = explode("\n", $data);
        $users = [];

        foreach ($data as $userData) {
            $userData = explode("|", $userData);
            $user = new User;
            $user->ip = $userData[0];
            $user->browser = $userData[1];
            $user->os = $userData[2];

            $users[] = $user;
        }

        return $users;
    }

    public function getVisits($users)
    {
        $data = file_get_contents(__DIR__ . "/../../logs/visits.log");
        $data = explode("\n", $data);
        $visits = [];
        $ips = [];

        foreach ($users as $user) {
            $ips[$user->ip] = $user;
        }

        foreach ($data as $visitData) {
            $visitData = explode("|", $visitData);

            if (!isset($ips[$visitData[2]])) {
                continue;
            }

            $visit = new UserVisit;
            $visit->visited_at = $visitData[0] . " " . $visitData[1];
            $visit->user_id = $ips[$visitData[2]]->id;
            $visit->url_from = $visitData[3];
            $visit->url_to = $visitData[4];

            $visits[] = $visit;
        }

        return $visits;
    }
}