<?php

namespace TestModule\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use TestModule\Model\Table\UserTable;

class ApiController extends AbstractActionController
{
    private $table;

    public function __construct(UserTable $table)
    {
        $this->table = $table;
    }

    public function fetchUsersAction()
    {

        $request = $this->getRequest();

        $start = $request->getQuery('start', 0);
        $limit = $request->getQuery('limit', 5);

        $ip = $this->getIpFilterFromRequest();
        $sort = $this->getSortFromRequest();

        $result = $this->table->fetchUsersWithVisitsAggregated($start, $limit, $sort['field'], $sort['direction'], $ip);

        $users = [];

        foreach ($result as $user) {
            $users[] = $user->toArray();
        }

        return new JsonModel([
            'users' => $users,
            'total' => $this->table->count($ip)
        ]);
    }

    protected function getIpFilterFromRequest()
    {
        $ip = null;
        $filters = JSON_DECODE($this->getRequest()->getQuery('filter', '[]'));

        foreach ($filters as $filter) {
            if ($filter->property === 'ip') {
                $ip = $filter->value;
            }
        }

        return $ip;
    }

    protected function getSortFromRequest()
    {
        $sort = [
            'field' => 'id',
            'direction' => 'desc'
        ];

        $sorts = JSON_DECODE($this->getRequest()->getQuery('sort', '[]'));

        foreach ($sorts as $requestSort) {
            $sort['field'] = $requestSort->property;
            $sort['direction'] = $requestSort->direction;
        }

        return $sort;
    }
}