<?php

namespace TestModule\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class TestModuleController extends AbstractActionController
{
    public function indexAction()
    {
        $view = new ViewModel([
        ]);
        $view->setTemplate('test/test/index')->setTerminal(true);

        return $view;
    }
}