<?php

namespace TestModule;

use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use TestModule\Model\User;
use TestModule\Model\UserVisit;
use TestModule\Model\Table\UserTable;
use TestModule\Model\Table\UserVisitTable;
use TestModule\Model\Table\UserTableGateway;
use TestModule\Model\Table\UserVisitTableGateway;

class Module implements ConfigProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                UserTable::class => function ($container) {
                    $tableGateway = $container->get(UserTableGateway::class);

                    return new UserTable($tableGateway);
                },

                UserVisitTable::class => function ($container) {
                    $tableGateway = $container->get(UserVisitTableGateway::class);

                    return new UserVisitTable($tableGateway);
                },

                UserTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new User());
                    return new TableGateway('user', $dbAdapter, null, $resultSetPrototype);
                },

                UserVisitTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new UserVisit());
                    return new TableGateway('user_visit', $dbAdapter, null, $resultSetPrototype);
                }
            ]
        ];
    }

    public function getControllerConfig()
    {
        return [
            'factories' => [
                Controller\TestModuleController::class => function ($container) {
                    return new Controller\TestModuleController();
                },
                Controller\ApiController::class => function ($container) {
                    return new Controller\ApiController($container->get(UserTable::class));
                }
            ]
        ];
    }
}