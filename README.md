# Тестовое задание

## Установка

1. git clone, cd в склонированную директорию
2. composer install
3. Настроить доступ к локальной бд в файле config/autoload/global.php
4. Импортировать бд из файла module/TestModule/test.sql
5. (Опционально) Добавить необходимые данные в файлы логов users.log и visits.log в директории module/TestModule/logs
6. Наполнить бд данными из логов, выполнив из корня проекта команду:
    php ./module/TestModule/bin/parselogs.php
7. Запустить сервер командой:
    php -S 0.0.0.0:8080 -t public public/index.php
8. Открыть в браузере страницу http://localhost:8080/
